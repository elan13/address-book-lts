Follow instruction to set up the app locally:

1. Clone the project. Run `git clone git@bitbucket.org:elan13/address-book-lts.git`
1. Run`composer install` from the inside of the cloned project
1. Clone `.env` to `.env.local` and add your own env variables
1. Run `php bin/console doctrine:migrations:migrate` to create tables for your env
1. Run `php bin/console server:run` and the app is accessible http://127.0.0.1:8000