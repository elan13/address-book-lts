<?php

namespace App\Controller;

use App\Entity\AddressBook;
use App\Form\AddressBookType;
use App\Repository\AddressBookRepository;
use App\Service\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Intl\Intl;
use Symfony\Component\Routing\Annotation\Route;

class AddressBookController extends Controller
{
    /**
     * @Route("/", name="address_book_index", methods={"GET"})
     */
    public function index(AddressBookRepository $addressBookRepository): Response
    {
        return $this->render('address_book/index.html.twig', [
            'address_books' => $addressBookRepository->findByAlphabetOrder(),
            'countryHelper' => Intl::getRegionBundle(),
        ]);
    }

    /**
     * @Route("/address-book/new", name="address_book_new", methods={"GET","POST"})
     */
    public function new(Request $request, FileUploader $fileUploader): Response
    {
        $addressBook = new AddressBook();
        $form = $this->createForm(AddressBookType::class, $addressBook);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {;
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($addressBook);
            $entityManager->flush();

            return $this->redirectToRoute('address_book_index');
        }

        return $this->render('address_book/new.html.twig', [
            'address_book' => $addressBook,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/address-book/{id}", name="address_book_show", methods={"GET"})
     */
    public function show(AddressBook $addressBook): Response
    {
        return $this->render('address_book/show.html.twig', [
            'address_book' => $addressBook,
            'countryHelper' => Intl::getRegionBundle(),
        ]);
    }

    /**
     * @Route("/address-book/{id}/edit", name="address_book_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, AddressBook $addressBook): Response
    {
        $form = $this->createForm(AddressBookType::class, $addressBook);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('address_book_index', [
                'id' => $addressBook->getId(),
            ]);
        }

        return $this->render('address_book/edit.html.twig', [
            'address_book' => $addressBook,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/address-book/{id}", name="address_book_delete", methods={"DELETE"})
     */
    public function delete(Request $request, AddressBook $addressBook): Response
    {
        if ($this->isCsrfTokenValid('delete'.$addressBook->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($addressBook);
            $entityManager->flush();
        }

        return $this->redirectToRoute('address_book_index');
    }
}
